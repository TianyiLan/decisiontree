/*
 * CSE 353 Project #1 ID3 Algorithm
 * Professor: Ritwik Banerjee
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package id3;

import java.util.ArrayList;

/**
 * This class serves as the element model, a node, for the tree class.
 * 
 * @author Tianyi Lan
 */
public class Node {
    //the name of the node

    private String nodeName;
    //the set of training data in this node
    private ArrayList<ArrayList<String>> nodeData;
    //all the remaining attribute names in this node
    private ArrayList<String> nodeAttributeNames;
    //all the values of the attribute classfier
    private ArrayList<String> nodeArrributeValues;
    //all the branches from this node
    private ArrayList<Node> nodeBranches;

    //constructor
    public Node() {
        nodeBranches = new ArrayList<>();
    }

    //all the getters and setters for each element
    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String Name) {
        nodeName = Name;
    }

    public ArrayList<ArrayList<String>> getNodeData() {
        return nodeData;
    }

    public void setNodeData(ArrayList<ArrayList<String>> data) {
        nodeData = data;
    }

    public ArrayList<String> getNodeAttributeNames() {
        return nodeAttributeNames;
    }

    public void setNodeAttributeNames(ArrayList<String> attributeNames) {
        nodeAttributeNames = attributeNames;
    }

    public ArrayList<String> getNodeArrributeValues() {
        return nodeArrributeValues;
    }

    public void setNodeArrributeValues(ArrayList<String> arrributeValues) {
        nodeArrributeValues = arrributeValues;
    }

    public ArrayList<Node> getNodeBranches() {
        return nodeBranches;
    }

    public void setNodeBranches(ArrayList<Node> branches) {
        nodeBranches = branches;
    }
}
