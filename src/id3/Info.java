/*
 * CSE 353 Project #1 ID3 Algorithm
 * Professor: Ritwik Banerjee
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package id3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class contains all the computational methods.
 * 
 * @author Tianyi Lan
 */
public class Info {

    //in order to calculate entropy
    //get proportion fisrt via counting the values
    //counting the number of each value of a given attribute
    //returns a Map<String, Integer> String = value name, Integer = corresponding number
    public static Map<String, Integer> countAttributeValues(ArrayList<ArrayList<String>> example, int attribute) {
        //store values and their numbers in a valuesAndNumbers for computation
        Map<String, Integer> valuesAndNumbers = new HashMap<>();

        //note that each column is an attribute, and each row is a set of values of all attribute          
        for (ArrayList<String> attribute_value : example) {
            String key_attribute_value = attribute_value.get(attribute);
            //if no match, meaning value first time shown, put a (value, number = 1) pair
            if (!valuesAndNumbers.containsKey(key_attribute_value)) {
                valuesAndNumbers.put(key_attribute_value, 1);
            } else //match, meaning value shown again, number is increased by 1
            {
                valuesAndNumbers.put(key_attribute_value, valuesAndNumbers.get(key_attribute_value) + 1);
            }
        }
        return valuesAndNumbers;
    }

    //compute the entropy of an example w.r.t a given attribute
    public static double calEntropy(ArrayList<ArrayList<String>> example, int attribute) {
        //init entropy
        double entropy = 0;
        //a valuesAndNumbers for computation
        Map<String, Integer> valuesAndNumbers = countAttributeValues(example, attribute);
        //size of example, cast to double for computation
        double example_size = (double) example.size();
        //start computing entropy  
        for (Entry<String, Integer> entry : valuesAndNumbers.entrySet()) {
            //proportion = value_number / example_size
            double value_number = (double) entry.getValue();
            double proportion = value_number / example_size;

            //note that change base formula: loga(b) = log(b)/log(a)
            //entropy = - Sum (proportion * log2(proportion)
            //        = - Sum( proportion * (log(proportion) / log(2))
            entropy += (-1) * proportion * (Math.log(proportion) / Math.log(2));
        }
        return entropy;
    }

    //in order to compute average entropy, first we need to get subset w.r.t a given value of an attribute
    public static ArrayList<ArrayList<String>> getSubset(ArrayList<ArrayList<String>> example, int attribute, String value) {
        //note that the set returned is a subset of example
        ArrayList<ArrayList<String>> subset = new ArrayList<>();
        //given a value of an attribute, split the example into subsets
        for (ArrayList<String> attribute_value : example) {
            //the subset will only contain the entries for which the selected attribute has the value 
            if (attribute_value.get(attribute).equals(value)) {
                subset.add(attribute_value);
            }
        }
        return subset;
    }

    //if classify the example by a given attribute, calculate the average entropy 
    public static double calAverageEntropy(ArrayList<ArrayList<String>> example, int attribute) {
        //count all value numbers of this attribute
        Map<String, Integer> valuesAndNumbers = countAttributeValues(example, attribute);
        //init average entropy  
        double averageEntropy = 0;
        //init subset entropy
        double subsetEntropy = 0;
        double subset_size = 0;
        double example_size = (double) example.size();
        ArrayList<ArrayList<String>> subset = new ArrayList<>();

        //if this given attribute will be classfying the example, compute the average entropy
        for (Entry<String, Integer> entry : valuesAndNumbers.entrySet()) {
            //get a subset for each value of this attribute
            subset = getSubset(example, attribute, entry.getKey());
            subset_size = subset.size();

            //porportion for which attribute has a certain value 
            double proportionAttributeValue = subset_size / example_size;

            //note that the target attribute will always be the last column of subset
            int targetAttribute = subset.get(0).size() - 1;

            //now for each subset, calculate its entropy
            Map<String, Integer> subsetValuesAndNumbers = countAttributeValues(subset, targetAttribute);
            for (Entry<String, Integer> subsetEntry : subsetValuesAndNumbers.entrySet()) {
                //reset subset entropy 
                subsetEntropy = 0;
                //calculate subset proportion
                double value_number = (double) subsetEntry.getValue();
                double subsetProportion = value_number / subset_size;
                //note that change base formula: loga(b) = log(b)/log(a)
                //entropy = - Sum (proportion * log2(proportion)
                //        = - Sum( proportion * (log(proportion) / log(2))
                subsetEntropy += (-1) * subsetProportion * (Math.log(subsetProportion) / Math.log(2));
            }
            //now we have all subset entropies, calculate the average entropy
            //average entropy = Sum(porportion with an attribute value * subset entropy)
            averageEntropy += proportionAttributeValue * subsetEntropy;
        }
        return averageEntropy;
    }

    //calculate information gain
    public static double calInfoGain(ArrayList<ArrayList<String>> example, int attribute) {
        double gain = 0;
        //note that the target attribute will always be the last column
        int targetAttribute = example.get(0).size() - 1;
        //information gain = entropy - average entropy
        gain = calEntropy(example, targetAttribute) - calAverageEntropy(example, attribute);

        return gain;
    }

    //to remove bias in info gain, need to use gain ratio instead of info gain
    public static double calSplitInfo(ArrayList<ArrayList<String>> example, int attribute) {
        double splitInfo = 0;
        //note that the split info actually is
        //the entropy of example w.r.t the values of the given attribute
        splitInfo = calEntropy(example, attribute);

        return splitInfo;
    }

    //now calculate the gain ratio
    public static double calGainRatio(ArrayList<ArrayList<String>> example, int attribute) {
        double gainRatio = 0;
        //gain ratio = info gain / split info
        gainRatio = calInfoGain(example, attribute) / calSplitInfo(example, attribute);

        return gainRatio;
    }

    //now calculate the max gain ratio, return its corresponding attribute
    public static int calMaxGainRatio(ArrayList<ArrayList<String>> example, ArrayList<String> attribute_name) {
        double maxGainRatio = 0;
        double currentGainRatio = 0;
        int currentAttribute = 0;
        int bestAttribute = 0;
        //for each attribute, compute the gain ratio
        for (currentAttribute = 0; currentAttribute < attribute_name.size(); currentAttribute++) {
            //if the current gain ratio is greater, then we find a greater gain ratio
            currentGainRatio = Info.calGainRatio(example, currentAttribute);
            if (currentGainRatio > maxGainRatio) {
                //update the max
                maxGainRatio = currentGainRatio;
                //the currentAttribute is the best attribute
                bestAttribute = currentAttribute;
            }
        }
        return bestAttribute;
    }

    //handle continuous-valued attribute
    public static void handleContinuousAttribute(ArrayList<ArrayList<String>> example, int attribute) {
        //init handled example
        ArrayList<ArrayList<String>> handledExample = new ArrayList<>(example.size());
        //continuous attribute
        ArrayList<String> conAttribute = new ArrayList<>();
        //target attribute
        ArrayList<String> targetAttribute = new ArrayList<>();
        //again, last column will be the target attribute
        int targetAttributeIndex = example.get(0).size() - 1;
        //threshold candidates
        ArrayList<Double> threshold = new ArrayList<>();
        //all the info gains
        ArrayList<Double> allInfoGain = new ArrayList<>();

        //make a instance of the original example
        for (ArrayList<String> element : example) {
            handledExample.add((ArrayList<String>) element.clone());
        }
        //first sort example according to the continuous attribute
        Comparator<ArrayList<String>> comparator = Comparator.comparing((ArrayList<String> l) -> l.get(attribute));
        Collections.sort(handledExample, comparator);

        //store continuous attribute values and target classfication
        for (ArrayList<String> attribute_value : handledExample) {
            conAttribute.add(attribute_value.get(attribute));
            targetAttribute.add(attribute_value.get(targetAttributeIndex));
        }

        //find all threshold candidates
        for (int i = 1; i < targetAttribute.size(); i++) {
            //if target classfication differs
            if (!targetAttribute.get(i - 1).equals(targetAttribute.get(i))) {
                //find middle-value
                double prev = Double.parseDouble(conAttribute.get(i - 1));
                double next = Double.parseDouble(conAttribute.get(i));
                threshold.add((prev + next) / 2);
            }
        }

        //we have threshold candidates
        for (int i = 0; i < threshold.size(); i++) {
            ArrayList<ArrayList<String>> temp = new ArrayList<>(handledExample.size());
            //make a instance of the sorted example
            for (ArrayList<String> element : handledExample) {
                temp.add((ArrayList<String>) element.clone());
            }

            for (ArrayList<String> attribute_value : temp) {
                if (Double.parseDouble(attribute_value.get(attribute)) < threshold.get(i)) {
                    attribute_value.set(attribute, "1");
                } else {
                    attribute_value.set(attribute, "0");
                }
            }
            allInfoGain.add(calInfoGain(temp, attribute));
        }

        //now we have all the info gains, find the threshold that max info gain
        double maxGain = Collections.max(allInfoGain);
        int index = allInfoGain.indexOf(maxGain);

        //now handle the example with the threshold
        for (ArrayList<String> attribute_value : handledExample) {
            if (Double.parseDouble(attribute_value.get(attribute)) < threshold.get(index)) {
                attribute_value.set(attribute, "1");
            } else {
                attribute_value.set(attribute, "0");
            }
        }

        example.clear();
        //copy result to example
        for (ArrayList<String> element : handledExample) {
            example.add((ArrayList<String>) element.clone());
        }
    }

    //returns the values of a given attribute
    public static ArrayList<String> getValues(ArrayList<ArrayList<String>> example, int attribute) {
        ArrayList<String> value = new ArrayList<>();
        //given an attribute, iterate through the exmaple
        for (ArrayList<String> attribute_value : example) {
            //if a value has not been added
            if (!value.contains(attribute_value.get(attribute))) {
                //add this value
                value.add(attribute_value.get(attribute));
            }
        }
        return value;
    }
}
