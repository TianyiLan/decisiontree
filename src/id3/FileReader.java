/*
 * CSE 353 Project #1 ID3 Algorithm
 * Professor: Ritwik Banerjee
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package id3;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class reads the data file and store the data
 * 
 * @author Tianyi Lan
 */
public class FileReader {
    //data file path
    public static final String DATA_FILE_PATH = "\\Java Program\\ID3\\data\\cse353-hw2-data.tsv";
   
    //return a list containing all attributes
    public static ArrayList<String> getDataAttributes() {
        //a list containing attribute names
        ArrayList<String> attribute_name = new ArrayList<>();
        //no attribute names in the data file,so add attribute names this way
        attribute_name.add("age");            //column 0       continuous
        attribute_name.add("workclass");      //1 
        attribute_name.add("fnlwgt");         //2              continuous
        attribute_name.add("education");      //3      
        attribute_name.add("education-num");  //4              continuous
        attribute_name.add("marital-status"); //5
        attribute_name.add("occupation");     //6
        attribute_name.add("relationship");   //7 
        attribute_name.add("race");           //8
        attribute_name.add("sex");            //9
        attribute_name.add("capital-gain");   //10            continuous
        attribute_name.add("capital-loss");   //11            continuous
        attribute_name.add("hours-per-week"); //12            continuous
        attribute_name.add("native-country"); //13            high-valued
        attribute_name.add("income"); //14

        return attribute_name;
    }

    //return a 2D list containing all the data
    public static ArrayList<ArrayList<String>> getDataSet() throws Exception {
        //a 2D dataset with rows and columns
        ArrayList<ArrayList<String>> dataSet = new ArrayList<>();
        BufferedReader input = new BufferedReader(new java.io.FileReader(DATA_FILE_PATH));
        String dataLine;
        
        //only read this number of lines
        int line = 4000;
        //start reading data file
        while ((dataLine = input.readLine()) != null && line> 0) {
            ArrayList<String> temp = new ArrayList<>();
            //get each column for each row
            String[] rows = dataLine.split("\t");
            temp.addAll(Arrays.asList(rows));
            dataSet.add(temp);
            
            line--;
        }
        return dataSet;
    }
}
