/*
 * CSE 353 Project #1 ID3 Algorithm
 * Professor: Ritwik Banerjee
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package id3;

import java.util.ArrayList;

/**
 * This is main driver class.
 * 
 * @author Tianyi Lan
 */
public class ID3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
        //first read data file, and populate data
        ArrayList<ArrayList<String>>data = FileReader.getDataSet();
        ArrayList<String> dataAttribute = FileReader.getDataAttributes();

        //handle continuous attributes 0, 2, 4, 10, 11, 12
        Info.handleContinuousAttribute(data, 0);
        Info.handleContinuousAttribute(data, 2);
        Info.handleContinuousAttribute(data, 4);
        Info.handleContinuousAttribute(data, 10);
        Info.handleContinuousAttribute(data, 11);
        Info.handleContinuousAttribute(data, 12);
        
        Tree dt = new Tree();
        Node root = dt.expandTree(data, dataAttribute);
        
        dt.printTree(root);
        
    }
    
}
