/*
 * CSE 353 Project #1 ID3 Algorithm
 * Professor: Ritwik Banerjee
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package id3;

import java.util.ArrayList;

/**
 * This class presents the tree strucsure for the project.
 * 
 * @author Tianyi Lan
 */
public class Tree {

    //expand the decision tree
    public Node expandTree(ArrayList<ArrayList<String>> example, ArrayList<String> attribute_name) {
        //first construct a node
        Node node = new Node();
        //set its subset and attribute names
        node.setNodeData(example);
        node.setNodeAttributeNames(attribute_name);

        //now for this node, decide which attribute is the best classfier
        //we know that the attribute with the max gain ratio is the best
        int bestAttribute = Info.calMaxGainRatio(example, attribute_name);
        //note that the name of a node is best attribute classfier
        node.setNodeName(attribute_name.get(bestAttribute));

        //we have the best classfier, then get the corresponding values for this node
        ArrayList<String> nodeArrributeValues = Info.getValues(example, bestAttribute);
        node.setNodeArrributeValues(nodeArrributeValues);

        //for each value of the best attribute
        for (int i = 0; i < nodeArrributeValues.size(); i++) {
            //get its subset of training data
            ArrayList<ArrayList<String>> nodeSubset = Info.getSubset(example, bestAttribute, nodeArrributeValues.get(i));

            //target attribute will be always the last column
            int targetAttribute = nodeSubset.get(0).size() - 1; // 
            ArrayList<String> targetAttributeValue = Info.getValues(nodeSubset, targetAttribute);
            //for branches
            Node branch = new Node();
            //if data in one class (pure)
            if (targetAttributeValue.size() == 1) {
                //name the node with the class
                branch.setNodeName(targetAttributeValue.get(0));
                
            }//still need to expand the tree, thus add a branch (not pure)
            else {
                //create a sub example for the branch
                ArrayList<ArrayList<String>> sub_example = new ArrayList<ArrayList<String>>();
                //the used attribute will not be in the sub training data
                for (ArrayList<String> attribute_value : nodeSubset) { 
                    ArrayList<String> temp = new ArrayList<String>();
                    for (int j = 0; j < attribute_value.size(); j++) {
                        if (j != bestAttribute) {
                            temp.add(attribute_value.get(j));
                        }
                    }
                    sub_example.add(temp);
                }
                 
                //create a sub attribue name set for branches
                ArrayList<String> sub_attribute_name = new ArrayList<String>();
                //used attribute will not be in the set
                for (String name : attribute_name) { 
                    if (!name.equals(attribute_name.get(bestAttribute))) {
                        sub_attribute_name.add(name);
                    }
                }
                //we have sub training data and new attribute set for branch
                //recusively expand the tree
                branch = expandTree(sub_example, sub_attribute_name); 
            }
            //link the branch
            node.getNodeBranches().add(branch);
        }
        return node;
    }

    public void printTree(Node root) {
        System.out.println("Node：" + root.getNodeName());
        if (!root.getNodeBranches().isEmpty()) {

            for (Node node : root.getNodeBranches()) {
                printTree(node);
            }
        }
    }
}
